/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcialfigura;


import javax.swing.JOptionPane;

/**
 *
 * @author Jose Luis
 */
class Hexagono {
   String Lado;
   String Apotema;
   
       public Hexagono(String Lado,String Apotema) {
        this.Lado = Lado;
        this.Apotema =Apotema;
        
       }

    public String getLado() {
        return Lado;
    }

    public void setLado(String Lado) {
        this.Lado = Lado;
    }

    public String getApotema() {
        return Apotema;
    }

    public void setApotema(String Apotema) {
        this.Apotema = Apotema;
    }

    
 
}